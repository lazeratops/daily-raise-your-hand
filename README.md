# Daily Raise Your Hand Demo + Tutorial

## Sample Work

- [Live Demo](https://wonderful-khorana-9315ea.netlify.app/)
- [Tutorial](TUTORIAL.md)

### Developer Tutorials Strategy

When deciding what tutorials and other content to work on next, I would consider the following approach (in rough order
of priority):

- Check out the internal Daily roadmap. If some parts of the API are changing or getting new versions, migration
  tutorials for existing customers might be in order. If new features are coming, there may be an opportunity to produce
  tutorials to launch alongside the new features. I put this in first place because it can be time-sensitive if we want
  to ensure the content is released in conjunction with a new feature or API change.
- See if there is any content that developers using Daily are actively asking for. Are there common issues being filed,
  or common questions being asked repeatedly in the community? If so, it may be worth creating content answering these
  common questions or problems. Looking at what Daily's users need the most as a matter of priority when producing
  content also seems in line with Daily's "customers first" value.
- Review the current content library against Daily's feature set. What existing features already have plenty of
  tutorials associated with them? Which are lacking?
- Are there any features Daily itself wants to actively encourage more adoption and/or visibility of? Can we think of
  unique/useful/fun ways to use such a features and create a tutorial around them to increase their visibility?
- Does Daily have any partners is would like to highlight who are doing cool stuff with the API? If a customer
  implemented a unique feature using Daily, it may be worth collaborating with them and "tutorializing" the feature.
  Aside from providing a useful tutorial for other devs, it can also help us cross-promote and highlight great usages of
  the Daily product.

### Work/Progress Notes

- I originally started with a _super_ basic join call UI, where the user just joins an existing call by default with no
  option to create a new call or enter a URL. However, I thought that for the sake of showing how easy existing Daily
  video calls can be to _extend_ with `daily-js`, I ended up using the somewhat more complicated call joining logic from
  the Daily Prebuilt demo. The tradeoff of course is some more complexity in the demo code; but all the actual feature
  work is isolated to `raise-hand.js` with a small setup call in `index.html`.
- I have knowingly deviated from the spec (sorry!) The assignment asked to:

  > Create a simple user interface in your own top-level page context (outside
  > the video call iframe), showing an updating list of the participants currently in
  > the call and whether they have their virtual hand raised or not.

  I did not do this and opted to only display participants who have raised their hand outside of the call frame. My
  reasoning is that the Daily Prebuilt already allows the user to see all participants in the call, and the ability to
  _highlight_ who has raised their hand in the call instead of duplicating already existing functionality seemed a little
  more useful. Of course in a real life setting, I'd run this proposal past relevant people and get feedback before
  deviating from the spec.

- I am doubtful whether the `setInterval()` approach to sending the local hand state to newly joined participants was
  the right way to go. I experienced missed hand state updates for new joiners without it and the best reasoning I could
  come up with was a timing issue with `participant-joined` events reaching existing participants (and them sending a
  message back) before the `app-message` event is fully initialized for the new joiner. Being pretty rusty with JS
  (for now!), I am very curious to find out if I actually did something wrong in the handler setup causing the missed
  messages, and what the proper way to fix it would be.

- I have primarily developed and tested this in Chromium 87.0.4280.88 on Fedora 33. I did run the demo on an iPad in
  Safari briefly as well, but out-of-scoped any extensive cross-browser compatibility testing.

- The Daily API docs have been wonderful! One tiny thing that confused me a bit which I had to figure out through trial
  and error: In the [app-message](https://docs.daily.co/reference#app-message) documentation, it is unclear which ID is
  being referenced by `fromId`: the `participant.user_id` or the `participant.session_id`. I wished this was explicitly
  defined in the docs.
