const handEmoji = String.fromCodePoint("0x270B");
console.log(handEmoji);

let userData = {
  sessionId: "",
  name: "",
  handRaised: false,
};

let pendingAck = {};

function handleError(eventName, e) {
  console.error(`failed to handle ${eventName} event:`, e);
}

function initRaiseYourHandListeners() {
  callFrame
    .on("joined-meeting", ryhHandleJoinedMeeting)
    .on("left-meeting", ryhHandleLeftMeeting)
    .on("participant-joined", ryhHandleParticipantJoined)
    .on("participant-left", ryhHandleParticipantLeft)
    .on("participant-updated", ryhHandleParticipantUpdated)
    .on("app-message", ryhHandleAppMessage);
}

/* Event handlers */

function ryhHandleJoinedMeeting() {
  try {
    let participant = callFrame.participants().local;
    userData.sessionId = participant.session_id;
    userData.name = participant.user_name;
    prepCameraOptionCheckbox();
  } catch (err) {
    handleError("joined-meeting", err);
  }
}

function ryhHandleLeftMeeting() {
  try {
    resetParticipants();
    resetLocal();
  } catch (err) {
    handleError("left-meeting", err);
  }
}

function ryhHandleParticipantJoined(e) {
  try {
    // Send local hand state to the newly joined participant
    const sessionId = e.participant.session_id;

    let attemptsLeft = 10;
    let interval = setInterval(trySendState, 1000);
    pendingAck.sessionId = interval;

    function trySendState() {
      sendLocalHandStateAppMessage(sessionId);
      attemptsLeft--;
      if (attemptsLeft === 0) {
        clearInterval(interval);
        console.warn(
          `failed to send local hand state to ${sessionId}; recipient never acknowledged`
        );
      }
    }
  } catch (err) {
    handleError("participant-joined", err);
  }
}

function ryhHandleParticipantLeft(e) {
  try {
    removeParticipant(e.participant.session_id);
  } catch (err) {
    handleError("participant-left", err);
  }
}

function ryhHandleParticipantUpdated(e) {
  try {
    const p = e.participant;
    if (p.local && p.user_name != userData.name) {
      userData.name = p.user_name;
      updateLocalParticipant();
    } else if (!p.local) {
      updateParticipantName(p.session_id, p.user_name);
    }
  } catch (err) {
    handleError("participant-updated", err);
  }
}

function ryhHandleAppMessage(e) {
  try {
    const data = e.data;
    console.log(
      `received msg of kind ${data.kind} from ${e.fromId}. Hand Raised: ${data.handRaised}`
    );
    if (data.kind === "ack") {
      if (pendingAck.sessionId) {
        clearInterval(pendingAck.sessionId);
        delete pendingAck.sessionId;
      }
      return;
    }
    if (data.kind === "update") {
      if (data.wantAck) {
        sendAck(e.fromId);
      }
      updateParticipantHandState(e.fromId, data.handRaised);
    }
  } catch (e) {
    handleError("app-message", err);
  }
}

function prepCameraOptionCheckbox() {
  // Disable video checkbox if the user's video track is unavailable
  const videoTrack = callFrame.participants().local.tracks.video;
  const cb = document.getElementById("do-toggle-hand-camera");
  if (videoTrack.state === "blocked") {
    cb.checked = false;
    cb.setAttribute("disabled", "true");
    return;
  }
  cb.removeAttribute("disabled");
}

/* Local participant Raise Your Hand functions */

function toggleRaiseHand() {
  userData.handRaised = !userData.handRaised;
  updateLocalParticipant();
  updateRaiseHandButton();
  maybeEnableCamera();
  sendLocalHandStateAppMessage(null);
}

function sendLocalHandStateAppMessage(recipientSessionId) {
  let recipient = "*";
  let wantAck = false;
  if (recipientSessionId) {
    recipient = recipientSessionId;
    wantAck = true;
  }
  callFrame.sendAppMessage(
    { kind: "update", wantAck: wantAck, handRaised: userData.handRaised },
    recipient
  );
}

function sendAck(recipientSessionId) {
  console.log(`sending ack to session ${recipientSessionId}`);
  callFrame.sendAppMessage({ kind: "ack" }, recipientSessionId);
}

function updateRaiseHandButton() {
  const button = document.getElementById("raise-hand");
  let action = "";
  if (userData.handRaised) {
    action = "Lower";
  } else {
    action = "Raise";
  }
  button.textContent = `${action} Hand ${handEmoji}`;
}

function updateLocalParticipant() {
  const localEle = document.getElementById("local");
  if (!userData.handRaised) {
    localEle.textContent = "";
    return;
  }
  let li = localEle.getElementsByTagName("li")[0];
  if (!li) {
    li = document.createElement("li");
  }
  li.textContent = `${userData.name} (You!) ${handEmoji}`;
  localEle.appendChild(li);
}

function maybeEnableCamera() {
  const checkBox = document.getElementById("do-toggle-hand-camera");
  const toggleCameraWithHand = checkBox.checked;
  if (!toggleCameraWithHand) {
    return;
  }
  const videoOn = callFrame.participants().local.video;
  if (userData.handRaised && !videoOn) {
    callFrame.setLocalVideo(true);
    return;
  }
}

function resetLocal() {
  userData.handRaised = false;
  updateLocalParticipant();
  updateRaiseHandButton();
}

/* Other participants' Raise Your Hand status functions */
function resetParticipants() {
  const otherParticipantsEle = document.getElementById("other-participants");
  otherParticipantsEle.textContent = "";
}

function addParticipant(id, name) {
  // This participant is already added (maybe we just haven't acknowledged their message yet)
  let li = document.getElementById(id);
  if (li) {
    return;
  }
  const otherParticipantsEle = document.getElementById("other-participants");
  li = document.createElement("li");
  otherParticipantsEle.appendChild(li);
  li.id = id;

  const nameSpan = document.createElement("span");
  nameSpan.className = "name";
  nameSpan.textContent = name;
  li.appendChild(nameSpan);

  const stateSpan = document.createElement("span");
  stateSpan.className = "state";
  stateSpan.textContent = handEmoji;
  li.appendChild(stateSpan);
}

function removeParticipant(id) {
  const li = document.getElementById(id);
  if (li) {
    li.remove();
  }
}

function updateParticipantHandState(id, handRaised) {
  if (!handRaised) {
    removeParticipant(id);
    return;
  }

  let participants = callFrame.participants();
  let p = participants[id];
  if (!p) {
    console.warn("could not find participant id " + id);
    return;
  }
  addParticipant(id, p.user_name);
  return;
}

function updateParticipantName(id, name) {
  console.log(`Updating participant name for ID: ${id}; name: ${name}`);
  const participantLi = document.getElementById(id);
  if (participantLi) {
    const participantNameEle = participantLi.getElementsByClassName("name");
    if (participantNameEle.length == 1) {
      participantNameEle[0].textContent = name;
    }
  }
}
